import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;


class Result {

    /*
     * Complete the 'minimumChunksRequired' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. LONG_INTEGER totalPackets
     *  2. 2D_LONG_INTEGER_ARRAY uploadedChunks
     */

    public static int minimumChunksRequired(long totalPackets, List<List<Long>> uploadedChunks) {
        List<Long> uploaded = new ArrayList<>();
        for(int i=0; i<uploadedChunks.size(); i++) {
            long min=uploadedChunks.get(i).get(0);
            long max=uploadedChunks.get(i).get(1);
            for(long j=min; j<=max; j++) uploaded.add(j);
        }
        List<List<Long>> nonUploadedChunks = new ArrayList<>();
        List<Long> nonUploadedChunk = new ArrayList<>();
        for(long i=1; i<=totalPackets; i++) {
            if(!uploaded.contains(i)) {
                nonUploadedChunk.add(i);
            }
            else {
                if(nonUploadedChunk.isEmpty()) continue;
                nonUploadedChunks.add(nonUploadedChunk);
                nonUploadedChunk = new ArrayList<>();
            }
        }
        if(!nonUploadedChunk.isEmpty()) nonUploadedChunks.add(nonUploadedChunk);
        System.out.println("Non uploaded packets:\n" + nonUploadedChunks);
        int minChunksUploadCount = 0;
        for(List<Long> nuc: nonUploadedChunks) {
            int length = nuc.size();
            String binary = Integer.toBinaryString(length);
            minChunksUploadCount += binary.length() - binary.replace("1", "").length();
        }
        return minChunksUploadCount;
    }
}


public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        long totalPackets = Long.parseLong(bufferedReader.readLine().trim());

        int uploadedChunksRows = Integer.parseInt(bufferedReader.readLine().trim());
        int uploadedChunksColumns = Integer.parseInt(bufferedReader.readLine().trim());
        
        // To run in hackerrank, just uncomment from line 68 to 84 and comment from 87 to 136
/*
        List<List<Long>> uploadedChunks = new ArrayList<>();

        IntStream.range(0, uploadedChunksRows).forEach(i -> {
            try {
                uploadedChunks.add(
                    Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .map(Long::parseLong)
                        .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        int result = Result.minimumChunksRequired(totalPackets, uploadedChunks);
        */
        
        
        List<List<Long>> uploadedChunks = new ArrayList<>();
        List<Long> uc = new ArrayList<>();
        /*
        sample use case 0
        10
        2
        2
        1 2
        9 10
        */
       
        // uc.add((long)1); uc.add((long)2);  uploadedChunks.add(uc); uc = new ArrayList<>(); uc.add((long)9); uc.add((long)10); uploadedChunks.add(uc);
        // int result = Result.minimumChunksRequired(10, uploadedChunks);
       
        /*
        end of sample use case 1
        */
        
        
        
        /*
        sample use case 1
        18
        1
        2
        9 17
        */
        
        // uc.add((long)9); uc.add((long)17); uploadedChunks.add(uc);
        // int result = Result.minimumChunksRequired(18, uploadedChunks);
    
        /*
        end of sample use case 1
        */
        
        
        /*
        sample use case 2
        20
        1
        2
        9 17
        */
        
        uc.add((long)9); uc.add((long)17); uploadedChunks.add(uc);
        int result = Result.minimumChunksRequired(20, uploadedChunks);
        
        /*
        end of sample use case 2
        */
        
        
        
        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
